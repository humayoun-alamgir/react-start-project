<?php

return [
    'defaults' => [
        'api_version' => '2021-01',
        'timeout_ms' => 60000,
    ],

    'auth_method' => env('SHOPIFY_AUTH_METHOD', 'app-bridge'),

    /**
     * App credentials - required for all types of app
     */
    'public_credentials' => [
        'api_key' => env('SHOPIFY_API_KEY'),
        'api_secret' => env('SHOPIFY_API_SECRET')
    ],

    /**
     * Store-level app credentials - required for private apps
     */
    'private_credentials' => [
        'shop_domain' => env('SHOPIFY_SHOP_DOMAIN'),
        'access_token' => env('SHOPIFY_ACCESS_TOKEN')
    ],

    'scopes' => [
        'read_script_tags', 'write_script_tags',
    ],

    'webhooks' => [
    ],

    'script_tags' => [
    ],

    'disable_routes' => false,
];
