# How to setup a Shopify app with Laravel and React
___by Humayoun Alamgir___

Dec 2021

This step-by-step guide is aimed to provide you with instructions on how to setup a new Shopify app project using Laravel, React, Polaris, and AppBridge. 

----

## Shopify
Create a Shopify app 
1. Logon to Shopify partners account and navigate to `Apps > Create app`
2. Select `Custom app` or `Public app` and fill out the form as shown below and click the `Create app` button (rememebr to replace `xyz` with your app name) 
    ```sh
    ##App name
        xyz
    ##App URL
        https://xyz.local/app
    ##Allowed redirection URL(s)
        https://xyz.local/oauth
        https://xyz.local/oauth/
        https://xyz.local/oauth/done
        https://xyz.local/oauth/done/
    ```

## Laravel
1. ### Create new project
    Create a fresh Laravel project and setup file permissions
    ```sh
    composer create-project --prefer-dist laravel/laravel xyz "6.*"
    ```
    ```sh
    cd project_name
    ```
    ```sh
    chmod 775 -R storage 
    ```
    ```sh
    sudo chown -R ?:www-data storage
    ```
    (replace ? with your username)

2. ### Setup locahost
    Setup localhost with https://xyz.local. OS specifc, so not covered here.

3. ### ENV
    Edit `.env` and update database credentials with `?` makred places with actual value
    ```sh
    DB_DATABASE=?
    DB_USERNAME=?
    DB_PASSWORD=?
    ```
    Also add the fllowing to the end of the file. Rememebr to replace `?` makred places with actual value
    ```hs
    ## Shopify-related environment
    SHOPIFY_AUTH_METHOD="app-bridge"
    SHOPIFY_API_KEY="?"
    SHOPIFY_API_SECRET="?"

    ## Mix-related environment
    MIX_APP_BASE_URL="${APP_URL}"
    MIX_API_BASE_URL="${MIX_APP_BASE_URL}/api/v1"
    MIX_ADMIN_API_URL="${MIX_API_BASE_URL}/admin"
    MIX_SHOPIFY_AUTH_METHOD="${SHOPIFY_AUTH_METHOD}"
    MIX_SHOPIFY_API_KEY="${SHOPIFY_API_KEY}"
    ```

4. ### Config
    Edit ```config/app.php``` and just below ```App\Providers\RouteServiceProvider::class,``` add
    ```sh
    Esc\Shopify\Providers\APIServiceProvider::class,
    ```
    Also edit ```config/esc_shopify.php``` and change ``` 'auth_method' => env('SHOPIFY_AUTH_METHOD', 'session')``` with
    ```sh
        'auth_method' => env('SHOPIFY_AUTH_METHOD', 'app-bridge')
    ```

5. ### Uer model
    Edit ```app/User.php```, and just below ``` use Illuminate\Notifications\Notifiable;``` add
    ```sh
    use Esc\Shopify\Traits\ShopifyUser;
    ```
    And just below ```use Notifiable;``` add 
    ```sh
    use ShopifyUser;
    ```

6. ### Shop model
    Create a new file ```app/Shop.php``` with the following code
    ```sh
    <?php

    namespace App;

    use Esc\Shopify\Models\ShopBase;
    use Illuminate\Database\Eloquent\Model;

    class Shop extends ShopBase
    {
        //
    }
    ```

7. ### Routes
    Edit ```routes/web.php``` and replace the code with the following
    ```sh
    Route::prefix('/app')->group(function () {
        Route::get('/', 'Admin\InitController@home');
        Route::get('{any}', 'Admin\InitController@home')->where(['any' => '.*']);
    });
    ```

8. ### Controller
    Create a new file ```app/Http/Controllers/Admin/InitController.php``` with the following code
    ```sh
    <?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;

    class InitController extends Controller
    {
        public function home(Request $request)
        {
            $shopDomain = $request->get('shop');
            $host = $request->get('host');

            if($host){
                return view('react')->with([
                    'apiKey' => config('esc_shopify.public_credentials.api_key'),
                    'shopDomain' => $shopDomain,
                    'host' => $host
                ]);
            }

            // app is not yet installed, so install it
            return redirect("/oauth?shop=$shopDomain");
        }
    }
    ```

9. ### View
    Create a new file ```resources/views/react.blade.php``` with the following code
    ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Admin</title>
    </head>
    <body>
        <div id="app" data-shop="{{$shopDomain}}" data-host="{{$host}}" data-api-key="{{$apiKey}}"></div>
        <script src="{{mix('js/app.js')}}"></script>
    </body>
    </html>
    ```

10. ### Finish up installation
    Run the follwoing commands

    ```sh
    composer require esc/shopify:v3.x-dev
    ```
    ```sh
    php artisan vendor:publish --provider=Esc\Shopify\Providers\APIServiceProvider
    ```
    ```sh
    php artisan migrate:fresh
    ```
    ```sh
    php artisan db:seed
    ```

    Thats all for Laravel side of things


## React
Installaing in configuring react is simple.

1. ### Install React
    To add React to the project run the following on a terminal
    ```sh
    npm install react react-dom react-router-dom@5.2.0 @shopify/polaris @shopify/app-bridge-react
    ```
    Please note: If you plan to use `withRouter` or `Switch` components, you are better off using version 5.2.0 of `react-router-dom`

2. ### Install App Bridge and Polaris
    Add App Bridge and Polaris dependencies to the project by running the following
    ```sh
    npm install @shopify/polaris @shopify/app-bridge @shopify/app-bridge-react @shopify/app-bridge-utils
    ```

3. ### Mix support
    Edit ```webpack.mix.js``` and replace the code block with the following
    ```sh
        mix.react('resources/js/app.js', 'public/js')
            .sass('resources/sass/app.scss', 'public/css');
    ```

4. ### Root JS
    Edit ```resources/js/app.js```, and just below ```require('./bootstrap');``` add
    ```sh
    require('./react');
    ```

    ----


    
## Struture
By now your project is pretty much ready for developement. You can choose to structure your project whatever way you feel comfortable with, but I strongly recomment to use the following struture.


1. ### index.jsx
    Create `resources/js/react/index.jsx` with the following content
    ```sh
    import React from 'react';
    import ReactDOM from 'react-dom';
    import { AppProvider } from "@shopify/polaris";
    import '@shopify/polaris/build/esm/styles.css';
    import { Provider } from '@shopify/app-bridge-react';
    import { BrowserRouter, Route, Switch } from "react-router-dom";
    import translations from "@shopify/polaris/locales/en.json";
    import ClientRouter from "./ClientRouter";
    import Navigation from "./Navigation";
    // pages
    import Dashboard from './pages/Dashboard';
    import Settings from './pages/Settings';
    // components
    import PageLayout from "./components/PageLayout";


    const App = ({ shop, host, apiKey }) => {
        const config = { apiKey: apiKey, shopOrigin: shop, host: host, forceRedirect: true };

        return (
            <BrowserRouter>
                <Provider config={config}>
                    <ClientRouter />
                    <AppProvider i18n={translations}>
                        <Navigation />
                        <PageLayout>
                            <Switch>
                                <Route path="/settings" component={Settings} />
                                <Route path="/" component={Dashboard} />
                            </Switch>
                        </PageLayout>
                    </AppProvider>
                </Provider>
            </BrowserRouter>
        );
    }

    export default App;

    const appElement = document.getElementById('app');
    if (appElement) {
        ReactDOM.render(<App {...(document.getElementById('app').dataset)} />, appElement);
    }
    ```
2. ### ClientRouter.jsx
    Create `resources/js/react/ClientRouter.jsx` with the following code
    ```sh
    import React from 'react';
    import {withRouter} from "react-router-dom"
    import {ClientRouter as AppBridgeClientRouter} from '@shopify/app-bridge-react';

    function ClientRouter(props) {
        const {history} = props;
        return (
            <AppBridgeClientRouter history={history} />
        )
    }

    export default withRouter(ClientRouter);
    ```

3. ### NavigationMenu.jsx
    Create `resources/js/react/NavigationMenu.jsx` with the following code
    ```sh
    import {AppLink, NavigationMenu} from '@shopify/app-bridge/actions';
    import {useAppBridge} from '@shopify/app-bridge-react';
    import {useLocation} from 'react-router-dom';

    function AppNavigation() {
        const app = useAppBridge();

        const location = useLocation();

        const dashboard = AppLink.create(app, {
            label: 'Dashboard',
            destination: '/',
        });

        const settings = AppLink.create(app, {
            label: 'Settings',
            destination: '/settings',
        });
        const navigationMenu = NavigationMenu.create(app, {
            items: [dashboard, settings],
        });

        switch (location.pathname) {
            case "/":
                navigationMenu.set({active: dashboard});
                break;
            case "/settings":
                navigationMenu.set({active: settings});
                break;
            default:
                navigationMenu.set({active: dashboard});
        }

        return null
    }

    export default AppNavigation;
    ```
4. ### http.jsx

    Create `resources/js/react/http.jsx` using below content

    ```sh
    import axios from 'axios';
    import { getSessionToken } from '@shopify/app-bridge-utils';
    import { createApp } from '@shopify/app-bridge';

    const { dataset } = document.getElementById('app');
    const app = createApp({ apiKey: dataset.apiKey, shopOrigin: dataset.shop, host: dataset.host, forceRedirect: true });

    const apiInstance = axios.create({
        baseURL: '/',
        timeout: 5000,
    });

    apiInstance.interceptors.request.use(function (config) {
        return getSessionToken(app).then( token => {
            config.headers['Authorization'] = `Bearer ${token}`;
            return config;
        });
    }, function (error) {
        return Promise.reject(error);
    });

    export class ApiClient {
        http = apiInstance;
    }

    export const api = new ApiClient();
    ```

5. ### PageLayout.jsx
    Create `resources/js/react/components/PageLayout.jsx` with the following code
    ```sh
    import React from "react";
    import {Layout, Page} from "@shopify/polaris";

    function PageLayout({children}) {
        return (
            <Page>
                <Layout>
                    <Layout.Section>
                        {children}
                    </Layout.Section>
                </Layout>
            </Page>
        )
    }

    export default PageLayout;
    ```

6. ### Dashboard.jsx
    Create `resources/js/react/pages/Dashboard.jsx` with the following code
    ```sh
    import React from 'react';
    import {Page, Card} from '@shopify/polaris';
    import {Loading} from '@shopify/app-bridge-react';

    const Dashboard = () => {
        const loading = false;

        if (loading) return (
            <Loading/>
        )

        return (
            <Page title="Dashboard">
                <Card sectioned title="Your dashboard goes here..."></Card>
            </Page>
        )
    }

    export default Dashboard;
    ```

7. ### Settings.jsx
    Create `resources/js/react/pages/Settings.jsx` with the following code
    ```sh
    import React from 'react';
    import {Page, Card} from '@shopify/polaris';

    const Settings = () => {
        return (
            <Page title="Settings">
                <Card sectioned title="All settings goes here"></Card>
            </Page>
        )
    }

    export default Settings;
    ```

8. ### Compile
    Run 
    ```sh
    npm run watch
    ```

## Done
Congrats! you have completed all the necessary steps. Now go break a leg :)

