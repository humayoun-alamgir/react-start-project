<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class ShopifyAdminController extends Controller
{
    public function settings(Request $request)
    {
        $shop = $request->user()->shop;
        return response()->json($shop);
    }
}
