<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InitController extends Controller
{
    public function home(Request $request)
    {
        $shopDomain = $request->get('shop');
        $host = $request->get('host');

        if($host){
            return view('react')->with([
                'apiKey' => config('esc_shopify.public_credentials.api_key'),
                'shopDomain' => $shopDomain,
                'host' => $host
            ]);
        }

        // app is not yet installed, so install it
        return redirect("/oauth?shop=$shopDomain");
    }
}
