import React from "react";
import {Layout, Page} from "@shopify/polaris";

function PageLayout({children}) {
    return (
        <Page>
            <Layout>
                <Layout.Section>
                    {children}
                </Layout.Section>
            </Layout>
        </Page>
    )
}

export default PageLayout;
