import {AppLink, NavigationMenu} from '@shopify/app-bridge/actions';
import {useAppBridge} from '@shopify/app-bridge-react';
import {useLocation} from 'react-router-dom';

function Navigation() {
    const app = useAppBridge();

    const location = useLocation();

    const dashboard = AppLink.create(app, {
        label: 'Dashboard',
        destination: '/',
    });

    const settings = AppLink.create(app, {
        label: 'Settings',
        destination: '/settings',
    });
    const navigationMenu = NavigationMenu.create(app, {
        items: [dashboard, settings],
    });

    switch (location.pathname) {
        case "/":
            navigationMenu.set({active: dashboard});
            break;
        case "/settings":
            navigationMenu.set({active: settings});
            break;
        default:
            navigationMenu.set({active: undefined});
    }

    return null
}

export default Navigation;
