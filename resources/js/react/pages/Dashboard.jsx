import React from 'react';
import {Page, Card} from '@shopify/polaris';
import {Loading} from '@shopify/app-bridge-react';

const Dashboard = () => {
    const loading = false;

    if (loading) return (
        <Loading/>
    )

    return (
        <Page title="Dashboard">
            <Card sectioned title="Your dashboard goes here... :)"></Card>
        </Page>
    )

}

export default Dashboard;