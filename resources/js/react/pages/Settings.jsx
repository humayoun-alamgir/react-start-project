import React, {useState} from 'react';
import {Page, Card} from '@shopify/polaris';
import {Loading} from '@shopify/app-bridge-react';
import {api} from "../http";

const Settings = () => {

    const [loading,setLoading] = useState(false);
    const [shopDomain, setShopDomain] = useState();


    api.http.get('/api/v1/settings')
        .then( res => {
            setLoading(false);
            setShopDomain(res.data.shop_domain);
        });

    if (loading) return (
        <Loading/>
    )

    return (
        <Page title="Settings">
            <Card sectioned title="All settings goes here">
                API call example... 
                <p>Retrieving shop_domain from database:{shopDomain}</p>
            </Card>
        </Page>
    )
}

export default Settings;