import axios from 'axios';
import { getSessionToken } from '@shopify/app-bridge-utils';
import { createApp } from '@shopify/app-bridge';

const { dataset } = document.getElementById('app');
const app = createApp({ apiKey: dataset.apiKey, shopOrigin: dataset.shop, host: dataset.host, forceRedirect: true });

const apiInstance = axios.create({
    baseURL: '/',
    timeout: 5000,
});

apiInstance.interceptors.request.use(function (config) {
    return getSessionToken(app).then( token => {
        config.headers['Authorization'] = `Bearer ${token}`;
        return config;
    });
}, function (error) {
    return Promise.reject(error);
});

export class ApiClient {

    http = apiInstance;

}

export const api = new ApiClient();
