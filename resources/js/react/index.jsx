import React from 'react';
import ReactDOM from 'react-dom';
import { AppProvider } from "@shopify/polaris";
import '@shopify/polaris/build/esm/styles.css';
import { Provider } from '@shopify/app-bridge-react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import translations from "@shopify/polaris/locales/en.json";
import ClientRouter from "./ClientRouter";
import Navigation from "./Navigation";
// pages
import Dashboard from './pages/Dashboard';
import Settings from './pages/Settings';
// components
import PageLayout from "./components/PageLayout";


const App = ({ shop, host, apiKey }) => {
    const config = { apiKey: apiKey, shopOrigin: shop, host: host, forceRedirect: true };

    return (
        <BrowserRouter>
            <Provider config={config}>
                <ClientRouter />
                <AppProvider i18n={translations}>
                    <Navigation />
                    <PageLayout>
                        <Switch>
                            <Route path="/settings" component={Settings} />
                            <Route path="/" component={Dashboard} />
                        </Switch>
                    </PageLayout>
                </AppProvider>
            </Provider>
        </BrowserRouter>
    );
}

export default App;

const appElement = document.getElementById('app');
if (appElement) {
    ReactDOM.render(<App {...(document.getElementById('app').dataset)} />, appElement);
}
