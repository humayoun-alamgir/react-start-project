<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
</head>
<body>
    <div id="app" data-shop="{{$shopDomain}}" data-host="{{$host}}" data-api-key="{{$apiKey}}"></div>
    <script src="{{mix('js/app.js')}}"></script>
</body>
</html>
